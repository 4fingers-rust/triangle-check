use std::io;
use std::f64;

macro_rules! input {
    ($input_name:ident) => {
        let mut $input_name = String::new();
        match io::stdin().read_line(&mut $input_name) {
            Ok(_n) => {}
            Err(error) => println!("error: {}", error),
        }
    };
}

fn main() {
    println!("Please input length of 3 vertex a, b, c. Start with a. Tap ENTER to comfirm and input the next values.");

    input!(first_input);
    let a: f64 = string_to_f64(first_input);
    input!(second_input);
    let b: f64 = string_to_f64(second_input);
    input!(third_input);
    let c: f64 = string_to_f64(third_input);

    if a>0.0 && b>0.0 && c>0.0 {
        println!("\n[RESULT]");
        if is_triangle(a, b, c) {
            println!("Yes, it's a triangle.\n");

            println!("[TYPE OF TRIANGLE]");
            if is_right_triangle(a, b, c) {
                println!("It's right trinagle.");
            }
            else if is_isosceles_triangle(a, b, c) {
                println!("It's isosceles trinagle.");
            }
            else if is_equilateral_triangle(a, b, c) {
                println!("It's quilateral triangle.");
            }
            else {
                println!("It's a normal triangle.");
            }

            println!("\n[CALCULATE]");
            let p: f64 = perimeter(a, b, c);
            let s: f64 = area(p, a, b, c);
            println!("Perimeter: {}\nArea: {}", p, s);
        }
        else {
            println!("Sorry, it isn't a triangle. You can try again.");
        }
    }
    else {
        println!("[WARNING!]:\nDo not try to input incorectly!");
        if a<=0.0 {
            println!("a was {}", a);
        }
        if b<=0.0 {
            println!("b was {}", b);
        }
        if c<=0.0 {
            println!("c was {}", c);
        }
    }
}

fn string_to_f64(x: String) -> f64 {
    x.trim().parse().unwrap()
}

fn is_triangle(a: f64, b: f64, c: f64) -> bool {
    if a+b>c || b+c>a || c+a>b {
        return true;
    }
    return false;
}

fn is_right_triangle(a: f64, b: f64, c: f64) -> bool{
    if a.powi(2)+b.powi(2)==c.powi(2) || b.powi(2)+c.powi(2)==a.powi(2) || c.powi(2)+a.powi(2)==b.powi(2) {
        return true;
    }
    return false;
}

fn is_isosceles_triangle(a: f64, b: f64, c: f64) -> bool {
    if a==b && a!=c || b==c && b!=a|| c==a && c!=b {
        return true;
    }
    return false;
}

fn is_equilateral_triangle(a: f64, b: f64, c: f64) -> bool {
    if a==b && a==c {
        return true;
    }
    return false;
}

fn perimeter(a: f64, b: f64, c: f64) -> f64 {
    a+b+c
}

fn area(p: f64, a: f64, b: f64, c: f64) -> f64 {
    (p*(p-a)*(p-b)*(p-c)).sqrt()
}